package util;

import static org.junit.Assert.*;

import java.util.Calendar;

import org.junit.Test;

import clock.ClockModel;

public class ClockTests {

	@Test
	public void testClockModel() {
		ClockModel clock = new ClockModel();
		
		Calendar now = Calendar.getInstance();
		int sec = now.get(Calendar.SECOND);
		int min = now.get(Calendar.MINUTE);
		int hour = now.get(Calendar.HOUR);
		assertEquals(sec,clock.getSeconds());
		assertEquals(min,clock.getMinutes());
		assertEquals(hour,clock.getHours());
		
	}

	@Test
	public void testClockModelIntIntInt() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetTime() {
		ClockModel c = new ClockModel();
		c.setTime(3, 30, 45);
		assertEquals(3, c.getHours());
		assertEquals(30, c.getMinutes());
		assertEquals(45, c.getSeconds());
		
		
	}
//
//	@Test
//	public void testIncrementSeconds() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testIncrementMinutes() {
//		fail("Not yet implemented");
//	}

}
